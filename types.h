typedef unsigned int   uint;
typedef unsigned short ushort;
typedef unsigned char  uchar;

typedef unsigned char uint8;
typedef signed char int8;

typedef unsigned short int uint16;
typedef signed short int int16;

typedef unsigned int uint32;
typedef signed int int32;

typedef unsigned long long int uint64;
typedef signed long long int int64;

typedef uint64 pml4e_t;